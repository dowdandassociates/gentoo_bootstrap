#!/bin/bash
./make_latest_public.sh us-east-1 x86_64 ebs
./make_latest_public.sh us-east-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 us-west-2
./copy_ebs_across_region.sh us-east-1 i386 us-west-2
./make_latest_public.sh us-west-2 x86_64 ebs
./make_latest_public.sh us-west-2 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 us-west-1
./copy_ebs_across_region.sh us-east-1 i386 us-west-1
./make_latest_public.sh us-west-1 x86_64 ebs
./make_latest_public.sh us-west-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 eu-west-1
./copy_ebs_across_region.sh us-east-1 i386 eu-west-1
./make_latest_public.sh eu-west-1 x86_64 ebs
./make_latest_public.sh eu-west-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 sa-east-1
./copy_ebs_across_region.sh us-east-1 i386 sa-east-1
./make_latest_public.sh sa-east-1 x86_64 ebs
./make_latest_public.sh sa-east-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 ap-northeast-1
./copy_ebs_across_region.sh us-east-1 i386 ap-northeast-1
./make_latest_public.sh ap-northeast-1 x86_64 ebs
./make_latest_public.sh ap-northeast-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 ap-southeast-1
./copy_ebs_across_region.sh us-east-1 i386 ap-southeast-1
./make_latest_public.sh ap-southeast-1 x86_64 ebs
./make_latest_public.sh ap-southeast-1 i386 ebs

./copy_ebs_across_region.sh us-east-1 x86_64 ap-southeast-2
./copy_ebs_across_region.sh us-east-1 i386 ap-southeast-2
./make_latest_public.sh ap-southeast-2 x86_64 ebs
./make_latest_public.sh ap-southeast-2 i386 ebs

